<?php

use Drupal\field_collection\Entity\FieldCollectionItem;
use Drupal\taxonomy\Entity\Term;

require_once('utils.inc');

function getNodeGalery($nid) {
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    $nodeTitle = $node->getTitle();
    $nodeDescription = $node->get('body')->getValue()[0]['value'];
    $nodeImage = 'https://dummyimage.com/250x250/ababab/ffffff&text=Img';
    $nodeURl = $node->url(); 
    if(!$node->get('field_picture')->isEmpty()){
        $pictureFC = $node->get('field_picture')->getValue()[0]['target_id'];
        $pictureID = FieldCollectionItem::load($pictureFC)->get('field_fc_image')->getValue()[0]['target_id'];
        $nodeImage = getFileUrl($pictureID);
    }
    return array('nodeTitle' => $nodeTitle, 'nodeDescription' => $nodeDescription, 'nodeImage' => $nodeImage, 'nodeUrl' => $nodeURl);
}

function getNodeAudio($nid) {
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    $nodeTitle = $node->getTitle();
    $nodeDescription = $node->get('body')->getValue()[0]['value'];
    $audioID = $node->get('field_audio')->getValue()[0]['target_id'];
    $nodeAudio = getFileUrl($audioID);
    return array('nodeTitle' => $nodeTitle, 'nodeDescription' => $nodeDescription, 'nodeAudio' => $nodeAudio);
}

function getNodeArchivo($nid) {
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    $nodeTitle = $node->getTitle();
    $nodeDescription = $node->get('body')->getValue()[0]['value'];
    $archivoID = $node->get('field_pdf')->getValue()[0]['target_id'];
    $nodeArchivo = getFileUrl($archivoID);
    return array('nodeTitle' => $nodeTitle, 'nodeDescription' => $nodeDescription, 'nodeArchivo' => $nodeArchivo);
}