<?php
use Drupal\field_collection\Entity\FieldCollectionItem;

require_once('utils.inc');

function getSpots($spots){
    $arraySpots = array();
    $cantSpots = count($spots);
    for($i = 0; $i < $cantSpots; $i++) {
        $indexFC = $spots[$i]['target_id'];
        $isEmpty = FieldCollectionItem::load($indexFC)->get('field_link')->isEmpty();
        if(!$isEmpty){
            $strUrl = FieldCollectionItem::load($indexFC)->get('field_link')->getValue()[0]['uri'];
            $spotUrl = explode(":",$strUrl);
            array_push($arraySpots, array(
                'tooltip' => FieldCollectionItem::load($indexFC)->get('field_tooltip')->getValue()[0]['value'],
                'url' => $spotUrl[1],
                'timeI' => FieldCollectionItem::load($indexFC)->get('field_initial_time')->getValue()[0]['value'],
                'timeF' => FieldCollectionItem::load($indexFC)->get('field_final_time')->getValue()[0]['value']
            ));
        }
    }
    return $arraySpots;
}