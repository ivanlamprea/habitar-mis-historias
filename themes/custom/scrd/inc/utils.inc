<?php

use Drupal\file\Entity\File;

function getFileUrl($id){
    $url = File::load($id)->url();
    return $url;
}