<?php

use Drupal\taxonomy\Entity\Term;

require_once('utils.inc');

function getTaxonomyTermHistory($id, $nid) {
    $term = Term::load($id);
    $termName = $term->getName();
    $termDescription = $term->getDescription();
    $termColor = '#cecece';
    $termParentLink = 'http://habitarmishistorias.gov.co';
    $termParentLinkName = 'Regresar';
    $termStamp = 'https://dummyimage.com/320x320/fff/cecece&text=stamp';

    if(!$term->get('field_color')->isEmpty()) {
        $termColor = $term->get('field_color')->getValue()[0]['value'];
    }
    
    if(!$term->get('field_stamp')->isEmpty())
    {
        $stampID = $term->get('field_stamp')->getValue()[0]['target_id'];
        $termStamp = getFileUrl($stampID);
    }

    if(!$term->get('field_parent_link')->isEmpty())
    {
        $termParentLink = $term->get('field_parent_link')->getValue()[0]['uri'];
        $termParentLinkName = $term->get('field_parent_link')->getValue()[0]['title'];
    }

    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'field_category' => $id,
        'type' => 'scrd_history'
    ]);
    $termNodes = array();
    foreach($nodes as $n) {
        $nodeState = false;
        if($n->id() == $nid){
            $nodeState = true;
        }
        $nodeUrl = $n->url();
        $nodeTitle = $n->getTitle();
        $nodeScreenshot = 'https://dummyimage.com/600x400/8f8f8f/fff&text=IMG';
        if(!$n->get('field_screenshot')->isEmpty()) {
            $imageID = $n->get('field_screenshot')->getValue()[0]['target_id']; 
            $nodeScreenshot = getFileUrl($imageID); 
        }
        $nodeCity = array();
        if(!$n->get('field_country_city')->isEmpty()){
            $nodeCityId = $n->get('field_country_city')->getValue()[0]['target_id'];
            $nodeCity = getTaxonomyTermCity($nodeCityId);
        }
        array_push($termNodes,  array(
                'title' => $nodeTitle,
                'screenshot' => $nodeScreenshot,
                'url' => $nodeUrl,
                'state' => $nodeState,
                'city' => $nodeCity
            )
        );
    }
    return array( 'termName' => $termName, 'termDescription' => $termDescription, 'termColor' => $termColor, 'termStamp' => $termStamp, 'linkParentName' => $termParentLinkName, 'linkParent' => $termParentLink, 'termNode' => $termNodes );
}


function getTaxonomyTermVideo($id, $nid){
    $term = Term::load($id);
    if($term === null){
        return array('error' => true);
    }
    $termSelectedName = $term->getName();

    $query = \Drupal::entityQuery('taxonomy_term');
    $query->condition('vid', 'scrd_history');
    //$query->sort('weight');
    $tids = $query->execute();
    $terms = Term::loadMultiple($tids);

    $termSiblings = array();
    foreach($terms as $t) {
        $termID = $t->id();
        $termName = $t->getName();
        array_push($termSiblings, array(
            'id' => $termID,
            'name' => $termName
        ));
    }
 
    $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'field_category' => $id,
        'type' => 'scrd_video'
    ]);

    $termNodes = array();

    foreach($nodes as $n) {
        $nodeVideoID = $n->get('field_video_interactive')->getValue()[0]['target_id'];
        $nodeVideo = getFileUrl($nodeVideoID);
        $spots = $n->get('field_spot')->getValue();
        array_push($termNodes,  array(
            'video' => $nodeVideo,
            'spots' => getSpots($spots)
        ));
    }

    return array('termName' => $termSelectedName ,'terms' => $termSiblings, 'termNode' => $termNodes, 'error' => false);
}

function getTaxonomyTermCity($cid){
    $term = Term::load($cid);
    
    
    $termPrefix =  $term->get('field_prefijo_country_prefix')->getValue()[0]['value'];
    $termCity = $term->get('field_city')->getValue()[0]['value'];
    $termCountryIcon = 'https://dummyimage.com/256x256/8f8f8f/fff&text=iconA';
    if(!$term->get('field_country_icon')->isEmpty()) {
        $imageID = $term->get('field_country_icon')->getValue()[0]['target_id']; 
        $termCountryIcon = getFileUrl($imageID); 
    }
    $termCityIcon = 'https://dummyimage.com/256x256/8f8f8f/fff&text=iconB';
    if(!$term->get('field_city_icon')->isEmpty()) {
        $imageID = $term->get('field_city_icon')->getValue()[0]['target_id']; 
        $termCityIcon = getFileUrl($imageID); 
    }
    return array('countryPrefix' => $termPrefix, 'cityName' => $termCity, 'countryIcon' => $termCountryIcon, 'cityIcon' => $termCityIcon );
}