<?php
use Drupal\field_collection\Entity\FieldCollectionItem;

require_once('utils.inc');

function getTracks($tracks){
    $arrayTracks = array();
    $cantTracks = count($tracks);

    for($i = 0; $i < $cantTracks; $i++){
        $indexFC = $tracks[$i]['target_id'];
        if(FieldCollectionItem::load($indexFC)->get('field_file_track')->isEmpty()){
            continue;
        }else {
            $trackID = FieldCollectionItem::load($indexFC)->get('field_file_track')->getValue()[0]['target_id'];
            $trackFile = getFileUrl($trackID);
            $langCode = FieldCollectionItem::load($indexFC)->get('field_lang')->getValue()[0]['value'];
            $langName = '';

            switch($langCode){
                case "ar":
                    $langName = 'Arabic';
                break;
                case "zh":
                    $langName = 'Chinese';
                break;
                case "en":
                    $langName = 'English';
                break;
                case "fr":
                    $langName = 'French';
                break;
                case "de":
                    $langName = 'German';
                break;
                case "it":
                    $langName = 'Italian';
                break;
                case "ja":
                    $langName = 'Japanese';
                break;
                case "ko":
                    $langName = 'Korean';
                break;
                case "pt":
                    $langName = 'Portuguese';
                break;
                case "ru":
                    $langName = 'Russian';
                break;
                case "es":
                    $langName = 'Spanish';
                break;
                case "tr":
                    $langName = 'Turkish';
                break;
            }

            
            array_push($arrayTracks, array(
                'urlTrack' => $trackFile,
                'langCode' => $langCode,
                'langName' => $langName
            ));
        }
    }

    return $arrayTracks;
}