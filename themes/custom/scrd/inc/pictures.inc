<?php
use Drupal\field_collection\Entity\FieldCollectionItem;

require_once('utils.inc');

function getPictures($pictures) {
    $arrayPictures = array();
    
    $cantPictures = count($pictures);
    for($i = 0; $i < $cantPictures; $i++) {
        $indexFC = $pictures[$i]['target_id'];
        $isEmpty = FieldCollectionItem::load($indexFC)->get('field_fc_image')->isEmpty();
        if(!$isEmpty){
            $pictureID = FieldCollectionItem::load($indexFC)->get('field_fc_image')->getValue()[0]['target_id'];
            $pictureFile = getFileUrl($pictureID);
            $pictureDescription = FieldCollectionItem::load($indexFC)->get('field_fc_description')->getValue()[0]['value'];
            
            array_push($arrayPictures, array(
                'picture' => $pictureFile,
                'description' => $pictureDescription
            ));
        }
    }
    return $arrayPictures;
}