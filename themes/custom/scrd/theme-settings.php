<?php 

use Drupal\Core\Form\FormStateInterface;

function scrd_form_system_theme_settings_alter(array &$form, FormStateInterface $form_state) {  
    $form['ft_basic'] = array(
        '#type' => 'details',
        '#title' => t('Basic Settings'),
        '#group' => 'scrd_settings',
    );

    $form['ft_basic']['theme_settings'] = $form['theme_settings'];
    $form['ft_basic']['logo'] = $form['logo'];
    $form['ft_basic']['favicon'] = $form['favicon'];

    unset($form['theme_settings']);
    unset($form['logo']);
    unset($form['favicon']);

    scrd_theme_settings_general($form, $form_state);
}

function scrd_theme_settings_general(&$form, &$form_state){
    _scrd_theme_frontpage($form);
}

function _scrd_theme_frontpage(&$form){
    $form['scrd_settings']['frontpage'] = array(
        '#type'  => 'details',
        '#title' => t('Front Page'),
        '#group' => 'scrd_settings',
    ); 

    $form['scrd_settings']['frontpage']['scrd_slogan'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Slogan'),
        '#default_value' => theme_get_setting('scrd_slogan'),
        '#description'   => t("Place this text in the widget spot on your site."),
    );

    $form['scrd_settings']['frontpage']['scrd_carrousel'] = array(
        '#type'                           => 'managed_file',
        '#default_value'                  => theme_get_setting('scrd_carrousel'),
        '#multiple'                       => TRUE,
        '#description'                    => t('Allowed extensions: gif png jpg jpeg'),
        '#upload_location'                => 'public://images/slider/',
        '#upload_validators'              => [
            'file_validate_extensions'         => array('gif png jpg jpeg'),
            'file_validate_size'               => array(25600000)
        ],
        '#title'                          => t('Carrousel'),
    ); 
}

