'use strict'

const api = '/api/history/';
const historyService = {};

historyService.viewed = (_id) => {
    return new Promise((resolve, reject) => {
        let request = new XMLHttpRequest();
        request.open('GET', api + _id, true);

        request.onload = () => {
            if (request.status >= 200 && request.status < 400) {
                let data = JSON.parse(request.responseText);
                resolve(data);
            } else {
                reject(Error('Error de respuesta'));
            }
        }

        request.onerror = () => {
            reject(Error('Error de petición'));
        }
        request.send();
    });
}