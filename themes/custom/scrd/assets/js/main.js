'use strict';

const eventPreload = new Event('preloadend');

const objectImages = {
    menu:  [
        '/themes/custom/scrd/assets/img/fdo/advierte.jpg',
        '/themes/custom/scrd/assets/img/fdo/fdoCreaciones.png',
        '/themes/custom/scrd/assets/img/fdo/fdoDocumentales.png',
        '/themes/custom/scrd/assets/img/fdo/fdoExperimentales.png',
        '/themes/custom/scrd/assets/img/fdo/fdoInicio.png',
        '/themes/custom/scrd/assets/img/fdo/fdoPerfiles.png',
        '/themes/custom/scrd/assets/img/interfaz/audioOff.svg',
        '/themes/custom/scrd/assets/img/interfaz/audioOn.svg',
        '/themes/custom/scrd/assets/img/interfaz/bkgActual.gif',
        '/themes/custom/scrd/assets/img/interfaz/corona.gif',
        '/themes/custom/scrd/assets/img/interfaz/hoverBotonSeccion.gif',
        '/themes/custom/scrd/assets/img/interfaz/logoAlcaldia.png',
        '/themes/custom/scrd/assets/img/interfaz/logoIndexPos.gif'
    ],
    section: []
}

function preload(_page) {
    let tmpImg;
    let countImage = 0;
    objectImages[_page].forEach((_item, _index) => {
        
        tmpImg = new Image();
        tmpImg.onload = function(){
            countImage++;
            if(countImage === objectImages[_page].length)
            {
                console.log('imagenes precargadas');
                window.dispatchEvent(eventPreload);
            }
        }
        tmpImg.onerror = function(){
            countImage++;
            return;
        }
        tmpImg.src = _item;
    
    });
}

function mute() {
    let audioBackground = document.getElementById('AudioBackground');
    let btnAudio = document.getElementById('audioBut');

    if (audioBackground.volume > 0.0) {
        audioBackground.pause();
        audioBackground.volume = 0;
        btnAudio.classList.remove("audioActivo");
        btnAudio.classList.add("audioInactivo");

    } else if (audioBackground.volume == 0) {
        audioBackground.play();
        audioBackground.volume = 0.4;
        btnAudio.classList.add("audioActivo");
        btnAudio.classList.remove("audioInactivo");
    }   
}


function viewed(_this,_id){
    let fr = Math.floor(_this.currentTime);

    if(fr == 0){
        _this.addEventListener('ended', function() {
            historyService.viewed(_id)
                .then(res => {
                    console.log(res);
                })
                .catch(err => console.log(err))
        })
    }
}

function openWindowComments() {
    let elementComment = document.getElementById('ScrdComments');
    elementComment.scrollTop = 0;
    elementComment.style.width = "475px";
}

function closeWindowComments() {
    let elementComment = document.getElementById('ScrdComments');
    elementComment.style.width = "0";
}

window.addEventListener('preloadend', function() {
    let preload = document.getElementById('Preload');   
    preload.style.display = 'none';
});
            

function openModal(_this){
    var pdfUrl = _this.getAttribute('data-pdf');
    document.getElementById('ObjectPdf').setAttribute('src', pdfUrl);
    document.getElementById('EmbedPdf').setAttribute('src', pdfUrl);
    jQuery('#ModalPdf').modal('show');
}