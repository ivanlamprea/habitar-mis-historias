((win)=> {
    let slides = document.querySelectorAll('.slide-background');
    let bar = document.querySelector('.slide-bar');
    let cant = slides.length - 1;
    let pos = cant;
    let time = 9; /** Tiempo en segundos */
    let timeInterval = (time * 0.7)* 1000; 
    function ready () {
       
        slides.forEach((_el) => {
            _el.style.animationDuration = time+'s';
            _el.addEventListener('animationstart', animationStart);
            _el.addEventListener('animationend', animationEnd);
        });
        slides[cant].classList.add('slide-animation');
        
    }

   
    function animationStart(){
        bar.style.transitionDuration = time+'s';
        bar.classList.add('active');
    }

    function animationEnd () {
        if(pos) {
            pos--;
        }
        else {
            pos = cant;
        }
    
        this.style.display = 'none';
        this.classList.remove('slide-animation');
        slides[pos].classList.add('slide-animation');
        bar.style.transitionDuration = '0s';
        bar.classList.remove('active');
        
        if(pos == 0) {
            setTimeout(() => {
                slides[cant].style.display = 'block';
                slides[cant].style.zIndex = '0';
            }, timeInterval);
        }

        if(pos == cant){
            let tope = cant-1;
            slides[cant].style.zIndex = '1';
            for(let i = tope; i >= 0; i--)
            {
                slides[i].style.display = 'block';
            }
        }
    }
    
    document.addEventListener("DOMContentLoaded", ready);
})(window);
