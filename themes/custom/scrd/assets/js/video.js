((win) => {
    let posV = 0;
    let video = document.getElementById('VideoScrd');
    let canvas = document.getElementById('CanvasScrd');
    let tooltip = document.getElementById('TooltipScrd');
    let tooltipText = document.querySelector('.tooltip-text');
    let videoIndex = 0;
    let progressBar = document.getElementById('fill'+[ videoIndex + 1 ]);

    function ready() {
        canvas.addEventListener('mousemove', canvasCursorMove);
        canvas.addEventListener('click', canvasRedirectLink);
        video.addEventListener('playing', startProgressUpdate);
        video.addEventListener('timeupdate', videoUpdate);
        if(isMenu){
            video.addEventListener('ended', videoEnded);
            video.src = videos[videoIndex].video;
        }
        video.play();
    }

    function checkSpot(_position, _ct) {
        let arrayTimeI = isMenu ? videos[videoIndex].spots[_position].timeI.split(':') : spots[_position].timeI.split(':');
        let arrayTimeF = isMenu ? videos[videoIndex].spots[_position].timeF.split(':') : spots[_position].timeF.split(':');
        let initialTime = parseInt(arrayTimeI[0]) * 60 + parseInt(arrayTimeI[1]); 
        let finalTime = parseInt(arrayTimeF[0]) * 60 + parseInt(arrayTimeF[1]);
        let spotsLength = isMenu ? videos[videoIndex].spots.length-1 : spots.length-1;
        
        if(_ct >= initialTime && _ct <= finalTime)
        {
            tooltipText.innerText = isMenu ? videos[videoIndex].spots[_position].tooltip : spots[_position].tooltip;
            canvas.style.display = 'block';
            tooltip.style.display= 'block';
            if(_ct == finalTime && _position < spotsLength) {
                posV++;
            }
        }
        else {
            canvas.style.display = 'none';
            tooltip.style.display= 'none';
        }
    }

    function videoUpdate() {
        let ct = parseInt(this.currentTime);
        let existSpots = isMenu ? videos[videoIndex].spots.length : spots.length;
        
        if(ct === 0)
            posV = 0;
        if(existSpots > 0)
            checkSpot(posV, ct);
    }

    function videoEnded() {
        let lengthVideos = videos.length - 1;
        progressBar.style.width = 0;
        if(videoIndex < lengthVideos)
            videoIndex++;   
        else
            videoIndex = 0;
        progressBar = document.getElementById('fill'+[ videoIndex + 1 ]);
        
        video.src = videos[videoIndex].video;
        video.play();
    }

    function startProgressUpdate(){
        let percentProgress =  (video.currentTime / video.duration) * 100;        
            progressBar.style.width = percentProgress + '%';
            t = win.requestAnimationFrame(startProgressUpdate);
    }

    function canvasCursorMove(e) {
        let mY = [e.clientY+15] + 'px';
        let mX = [e.clientX+10] + 'px';
        tooltip.style.left = mX;
        tooltip.style.top = mY;
    }

    function canvasRedirectLink() {
        let url = isMenu ? '/'+videos[videoIndex].spots[posV].url : '/'+spots[posV].url;
        win.location.href = url;
    }
    
    win.addEventListener('preloadend', function() {
        let audioBackground = document.getElementById('AudioBackground');
        audioBackground.play();
        audioBackground.volume = '0.4';
        if(videos.length > 0)
            ready();
    });

    if(!isMenu)
        ready();

})(window);