<?php
    namespace Drupal\scrd_history\Controller;
    use Drupal;
    use Drupal\node\Entity\Node;
    use Drupal\taxonomy\Entity\Term;
    use Drupal\file\Entity\File;

    class ScrdHistoryController {
        
        public function getAll() {
            $terms = Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree('scrd_history',0,NULL,TRUE);
            $arrayTerms = array();

            foreach($terms as $t) {
                
                $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
                    'field_category' => $t->id(),
                ]);
                $arrayNodes = array();
                foreach($nodes as $n) {
                    $nodeHistory = new NodeHistory();
                    $nodeHistory->title = $n->getTitle();
                    $nodeHistory->text = $n->get('field_text')->getValue()[0]['value'];
                    $nodeHistory->category = $n->get('field_category')->getValue()[0]['target_id'];
                    if($n->get('field_video')->isEmpty())
                    {
                        $video = 'Video not found';
                        $nodeHistory->video = $video;
                    }
                    else{
                        $videoID = $n->get('field_video')->getValue()[0]['target_id'];
                        $video = File::load($videoID)->url();
                        $nodeHistory->video = $video; 
                    }
        
                    if($n->get('field_screenshot')->isEmpty()) {
                        $image = 'https://dummyimage.com/600x400/8f8f8f/fff&text=IMG';
                        $nodeHistory->screenshot = $image;
                    }
                    else{
                        $imageID = $n->get('field_screenshot')->getValue()[0]['target_id'];    
                        $image = File::load($imageID)->url();
                        $nodeHistory->screenshot = $image;
                    }
                    
                    array_push($arrayNodes, $nodeHistory);
                }
                // Añadimos la logica de las Taxonomias 

                
                $termColor = '#cecece';
                if(!$t->get('field_color')->isEmpty()) {
                    $termColor = $t->get('field_color')->getValue()[0]['value'];
                }
                $termStamp = 'https://dummyimage.com/320x320/fff/cecece&text=stamp';
                if(!$t->get('field_stamp')->isEmpty())
                {
                    $stampID = $t->get('field_stamp')->getValue()[0]['target_id'];
                    $termStamp = getFileUrl($stampID);
                }


                $termHistory = new TermHistory(
                    $t->id(),
                    $t->getName(),
                    $t->getDescription(),
                    $termColor,
                    $termStamp,
                    $arrayNodes
                );
                
                array_push($arrayTerms ,$termHistory); 
            }
          
            return array(
                '#theme' => 'scrd-all',
                '#items' => $arrayTerms,
                '#title' => 'Historias de vida'
            );
        }

        public function test() {
            $items = array();
          
            return array(
                '#theme' => 'test',
                '#items' => $items,
                '#title' => 'Test'
            );
        }
    }

    class TermHistory {
        public $id;
        public $title;
        public $description;
        public $color;
        public $stamp;
        public $nodesHistory;

        public function __construct($id, $t, $d, $c, $s, $n){
            $this->id = $id;
            $this->title = $t;
            $this->description = $d;
            $this->color = $c;
            $this->stamp = $s;
            $this->nodesHistory = $n;
        }
    }

    class NodeHistory {
        public $title;
        public $text;
        public $category;
        public $video;
        public $screenshot;
    }