<?php
namespace Drupal\scrd_history\Controller;

use Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ScrdApiController {
    public function renderApi(Request $req){
        $reqId = $req->attributes->get('id');
        return new JsonResponse(
            [
                'data' => $this->getResults($reqId),
                'method' => 'GET',
            ]
        );
    }

    private function getResults($nodeID) {
        $node = Node::load($nodeID);
        if($node != null){
            $counter = $node->get('field_counter')->getValue()[0]['value'];
            $increment = intval($counter) + 1; 
        
            $node->set('field_counter', $increment);
            $node->save();

            return "The response have been successful";
        }
        return "Internal Error: Node doesn't exists";
    }
}