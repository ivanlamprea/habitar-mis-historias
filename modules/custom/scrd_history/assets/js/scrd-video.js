((win) => {
    let buttons = document.querySelectorAll('.button-card');
    let videos = document.querySelectorAll('video');
    function ready() {
        buttons.forEach((_el) => {
            _el.addEventListener('click', playVideo);
        })
    }

    function playVideo(){
        let id = this.getAttribute('data-id');
        let tit = this.getAttribute('data-title');
        let tex = this.getAttribute('data-text');
        let vid = this.getAttribute('data-url');
        
        let elText =  document.getElementById('TextHistory' + id);
        let elTitle = document.getElementById('TitleHistory' + id);
        let elVideo = document.getElementById('VideoHistory' + id);
       

        videos.forEach((_el) => {
            _el.pause();
        })

        elTitle.innerHTML = tit;
        elText.innerHTML = tex;
        elVideo.src = vid;
        elVideo.play();

        buttons.forEach((_el) => {
            _el.classList.remove('active');
        })
        this.classList.add('active');
    }

    document.addEventListener("DOMContentLoaded", ready);
})(window);